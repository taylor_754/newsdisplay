window.loadScoreboard = function(data)
{
	window.init (JSON.parse(decodeURIComponent(escape(JSON.stringify(data)))));
	console.log ("Inside of loadScoreboard");
	//console.log(data);
}

//http://mlb.mlb.com/gdcross/components/game/mlb/year_"+year+"/month_"+month+"/day_"+day+"/master_scoreboard.json


app.controller('nhlController', function($scope, $sce, $window, $http) {

	//$http.jsonp($sce.trustAsResourceUrl("http://live.nhl.com/GameData/RegularSeasonScoreboardv3.jsonp"),
    //{headers : {"Content-Type" : "application/json; charset=UTF-8", "Accept": "application/json;charset=UTF-8"}}).catch(function (err){console.log ("Failed to load file"); console.log (err)});

	//$http.jsonp($sce.trustAsResourceUrl("feb24_2018.jsonp"), {headers : {"Content-Type" : "application/json; charset=UTF-8", "Accept": "application/json;charset=UTF-8"}}).then(function(success) {
    $http.jsonp($sce.trustAsResourceUrl("http://live.nhl.com/GameData/RegularSeasonScoreboardv3.jsonp"), {headers : {"Content-Type" : "application/json; charset=UTF-8", "Accept": "application/json;charset=UTF-8"}}).then(function(success) {
      //$scope.articles = success.data.articles;
       //console.log ($scope.articles);
       console.log ("success");
   }, function (error){console.log ("failure");});

	$window.init = function (data){
		console.log (data);
		console.log ("Inside $window.init");

		$scope.relevantGames = [];
		$scope.playingGames = [];
		$scope.playedGames = [];
		$scope.futureGames = [];
		var showScores = (new Date().getHours() >= 12)?true:false;

		for (var i = 0; i < data.games.length; i++)
    	{
    		var game = data.games[i];
    		if (game.bs.indexOf ("FINAL") != -1)
    		{
    			if (game.gs == 1)
    			{
    				game.time = game.ts;
	    			$scope.futureGames.push (game);
    			}
	    		else 
	    		{
	    			game.time = game.bs;
	    			$scope.playingGames.push (game);
	    		}
    		}
    		else
    		{
    			game.time = (game.ts == "TODAY")?game.bs:game.ts;
	    		$scope.playedGames.push (game);
    		}
	    		
	    	if (showScores)
	    	{
	    		$scope.relevantGames = $scope.playedGames;
	    	}
	    	else
	    	{
	    		$scope.relevantGames = $scope.playingGames;
	    	}
    	}


		//$("#nhl-container").css ("width", $scope.relevantGames.length*160 + "px");
		var repeatCount = $scope.relevantGames.length - 4;
		var currentIteration = 1;
		console.log($scope.relevantGames.length);
		//$("#nhl-container").addClass("ticker");

		startAnimation ("hockey", repeatCount);

	}

});